﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveThisCanvas : MonoBehaviour
{
    [SerializeField]
    private Camera mainCam;

    [SerializeField]
    private GameObject endGameCanvas;
    void Update()
    {
        if(mainCam.enabled == false)
        {
            endGameCanvas.SetActive(true);
        }
    }
}
