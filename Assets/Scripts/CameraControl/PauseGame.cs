﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseGame : MonoBehaviour
{
    [SerializeField]
    private Camera mainCam;

    [SerializeField]
    private Camera uiCam;

    [SerializeField]
    private Camera pauseCam;

    void Start()
    {
        Button Button = gameObject.GetComponent<Button>();
        Button.onClick.AddListener(ChangeCameraToMain);
    }
    void ChangeCameraToMain()
    {
        mainCam.enabled = true;
        pauseCam.enabled = false;
        uiCam.enabled = false;
    }

}
