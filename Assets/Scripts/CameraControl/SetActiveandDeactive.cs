﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveandDeactive : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy_01;

    [SerializeField]
    private GameObject playerShip;

    [SerializeField]
    private Camera uiCamera;

    [SerializeField]
    private Camera mainCamera;

    [SerializeField]
    private Camera pauseCam;

    [SerializeField]
    private GameObject pauseCanvas;

    [SerializeField]
    private Camera endGameCam;

    [SerializeField]
    private GameObject endGameCanvas;
    
    void Start()
    {
        GetComponent<SetActiveandDeactive>();
        enemy_01.SetActive(false);
        playerShip.SetActive(false);
        uiCamera.enabled = true;
        pauseCam.enabled = false;
        endGameCam.enabled = false;
        pauseCanvas.active = false;
        
    }
    private void Update()
    {
        if(uiCamera.enabled == false)
        {
            enemy_01.SetActive(true);
            playerShip.SetActive(true);
            GetComponent<SetActiveandDeactive>().enabled = false;
        }
        
    }
}
