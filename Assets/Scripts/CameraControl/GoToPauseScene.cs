﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToPauseScene : MonoBehaviour
{
    private int normalTimeScale = 1;

    private int timeStopScale = 0;

    [SerializeField]
    private Camera mainCam;

    [SerializeField]
    private Camera uiCam;

    [SerializeField]
    private Camera pauseCam;

    [SerializeField]
    private GameObject pauseCanvas;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = timeStopScale;
            mainCam.enabled = false;
            uiCam.enabled = false;
            pauseCam.enabled = true;
            pauseCanvas.active = true;
        }

        if (mainCam.enabled == true && pauseCam.enabled == false)
        {
            Time.timeScale = normalTimeScale;
        }
    }
}
