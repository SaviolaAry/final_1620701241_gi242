﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeCamera : MonoBehaviour
{
    [SerializeField]
    private Camera mainCam;

    [SerializeField]
    private Camera uiCam;

    [SerializeField]
    private Camera pauseCam;

    [SerializeField]
    private GameObject startUI;
    
    [SerializeField]
    private AudioSource audioSource;
    void Start()
    {
        uiCam.enabled = true;
        mainCam.enabled = false;
        pauseCam.enabled = false;
        Button Button = gameObject.GetComponent<Button>();
        Button.onClick.AddListener(ChangeCameraToMain);
    }

    void ChangeCameraToMain()
    {
        mainCam.enabled = true;
        uiCam.enabled = false;
        pauseCam.enabled = false;
        startUI.SetActive(false);
        audioSource.Stop();
    }
}
