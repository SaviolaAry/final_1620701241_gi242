﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableUI : MonoBehaviour
{
    [SerializeField]
    private GameObject pauseCanvas;

    [SerializeField]
    private GameObject exitCanvas;

    [SerializeField]
    private GameObject startUI;

    [SerializeField]
    private GameObject playerShip;

    void Start()
    {
        exitCanvas.SetActive(false);
    }

}
