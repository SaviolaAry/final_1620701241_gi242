﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitArea : MonoBehaviour
{
    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -11, 11f),
            Mathf.Clamp(transform.position.y, -9f, 9f), transform.position.z);
    }
}
