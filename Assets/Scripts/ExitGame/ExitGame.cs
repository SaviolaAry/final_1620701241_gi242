﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitGame : MonoBehaviour
{
    [SerializeField]
    private Button exitButton;

    void Start()
    {
        Button Button = gameObject.GetComponent<Button>();
        Button.onClick.AddListener(DoExit);
    }

    private void DoExit()
    {
        Debug.Log("Player has quit the game");
        Application.Quit();
    }
}
