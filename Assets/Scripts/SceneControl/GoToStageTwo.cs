﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToStageTwo : MonoBehaviour
{
    public void GoStageTwo()
    {
        SceneManager.LoadScene("1620701241_PlayerShip_Stage2");
    }
}
