﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoBackToMainScene : MonoBehaviour
{
    public void GoBackToMainMenu()
    {
        SceneManager.LoadScene("1620701241_PlayerShip");
    }
}
