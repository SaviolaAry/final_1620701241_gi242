﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RestartGame : MonoBehaviour
{
    [SerializeField]
    private Button button;
    private void Start()
    {
        Button Button = gameObject.GetComponent<Button>();
        Button.onClick.AddListener(ResetGame);
    }

    void ResetGame()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
