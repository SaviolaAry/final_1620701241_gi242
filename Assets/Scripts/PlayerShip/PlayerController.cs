﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace PlayerShip_1620701241
{
    public class PlayerController : MonoBehaviour
    {

        [SerializeField]
        float playerShipSpd = 10;

        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;

        private Vector2 playerInputInfo = Vector2.zero;

        void Start()
        {
            Application.targetFrameRate = -1;
        }

        void FixedUpdate()
        {

            MoveWithKeyboard();
            //MoveWithMouse();

        }
        /*
        private void MoveWithMouse()
        {

            playerInputInfo = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
            playerInputInfo = playerInputInfo.normalized;
            
            var newX = transform.position.x + playerInputInfo.x*Time.deltaTime*playerShipSpd;
            var newY = transform.position.y + playerInputInfo.y*Time.deltaTime*playerShipSpd;

            transform.position = new Vector2(newX, newY);
        }
        */
        private void MoveWithKeyboard()
        {

            playerInputInfo = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            playerInputInfo = playerInputInfo.normalized;

            var newX = transform.position.x + playerInputInfo.x * Time.deltaTime * playerShipSpd;
            var newY = transform.position.y + playerInputInfo.y * Time.deltaTime * playerShipSpd;

            transform.position = new Vector2(newX, newY);

        }

    }
}
