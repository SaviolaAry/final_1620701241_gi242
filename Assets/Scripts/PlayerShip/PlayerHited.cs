﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHited : MonoBehaviour
{
    [SerializeField]
    private GameObject enemyShip;

    [SerializeField]
    private GameObject playerShip;

    [SerializeField]
    private Camera mainCam;

    [SerializeField]
    private Camera pauseCam;

    [SerializeField]
    private Camera startCam;

    [SerializeField]
    private Camera endGameCam;

    [SerializeField]
    private GameObject endGameCanvas;

    [SerializeField]
    private Text hpShowOnScreen;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip playerShipExplodeSound;

    [SerializeField]
    private GameObject nextStageButton;

    private int totalScore;

    private int newGenScore;

    [SerializeField]
    private int Hp = 40;
    private void OnTriggerEnter(Collider other)
    {
        Hp--;
        Debug.Log(Hp);
        hpShowOnScreen.text = "PLAYER HP : " + Hp;
        endGameCanvas.SetActive(true);
        if (Hp <= 0)
        {
            audioSource.Play();
            playerShip.SetActive(false);
            mainCam.enabled = false;
            pauseCam.enabled = false;
            startCam.enabled = false;
            endGameCam.enabled = true;
            enemyShip.SetActive(false);
            nextStageButton.SetActive(false);

        }

    }
}
