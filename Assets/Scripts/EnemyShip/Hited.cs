﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hited : MonoBehaviour
{
    [SerializeField]
    private GameObject enemyShip;

    [SerializeField]
    private GameObject playerShip;

    [SerializeField]
    private Camera mainCam;

    [SerializeField]
    private Camera pauseCam;

    [SerializeField]
    private Camera startCam;

    [SerializeField]
    private Camera endGameCam;

    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private Text hpShowOnScreen;

    [SerializeField]
    private Text finalTextScore;

    [SerializeField]
    private GameObject endgamecanvas;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip shipExplodeSound;


    private int totalScore;

    private int newGenScore;

    [SerializeField]
    private int Hp = 40;
    private void OnTriggerEnter(Collider other)
    {
        
        Debug.Log("Hit Detected");
        
        Hp--;
        Debug.Log(Hp);
        
        newGenScore = Random.Range(1, 3);
        totalScore = totalScore + newGenScore;
        Debug.Log(totalScore);
        scoreText.text = "Score : " + totalScore;
        finalTextScore.text = "Score : " + totalScore;
        hpShowOnScreen.text = "ENEMY HP : " + Hp; 
        endgamecanvas.SetActive(true);
        
        if(Hp <= 0)
        {
            audioSource.Play();
            mainCam.enabled = false;
            pauseCam.enabled = false;
            startCam.enabled = false;
            endGameCam.enabled = true;
            Destroy(enemyShip);
            playerShip.SetActive(false);
        }
    }
}
