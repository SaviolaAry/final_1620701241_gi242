﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EnemyShip_1620701241
{
    public class EnemyController : MonoBehaviour

    {

        [SerializeField]
        Transform playerTransform;

        [SerializeField]
        float enemyShipSpeed;

        private float minDistanceToplayer = 2.0f;
        
        void Update()
        {

            Vector2 displacementFromPlayer = playerTransform.position - transform.position;
            Vector2 directionToPlayer = displacementFromPlayer.normalized;
            Vector2 enemyVelocity = directionToPlayer * enemyShipSpeed;

            if (displacementFromPlayer.magnitude > minDistanceToplayer)
            {

                transform.Translate(enemyVelocity * Time.deltaTime);

            }
        }
    }
}

