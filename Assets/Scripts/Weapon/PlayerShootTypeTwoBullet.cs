﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShootTypeTwoBullet : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip fireSFX;

    [SerializeField]
    private float volume;

    [SerializeField]
    private float speed;

    [SerializeField]
    private float tilt;

    [SerializeField]
    private GameObject typeTwoAmmo;

    [SerializeField]
    private Transform bulletSpawner;

    private float fireRate = 1;

    private float nextFire;
    // Start is called before the first frame update
    void Update()
    {
        if (Input.GetButton("Fire2") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(typeTwoAmmo, bulletSpawner.position, bulletSpawner.rotation);
            audioSource.PlayOneShot(fireSFX, volume);
        }
    }
}
