﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootBullet : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip fireSFX;

    [SerializeField]
    private float volume;

    [SerializeField]
    private float speed;

    [SerializeField]
    private float tilt;

    [SerializeField]
    private float fireRate;

    [SerializeField]
    private float setFireTimer = 2;

    [SerializeField]
    private float fireTime = 0;

    [SerializeField]
    private GameObject enemyShot;

    [SerializeField]
    public Transform bulletSpawner;

    private float nextFire;

    // Start is called before the first frame update
    void Update()
    {
        fireTime -= Time.deltaTime;
        if(fireTime <= 0)
        {
            nextFire = Time.deltaTime + fireRate;
            Instantiate(enemyShot, bulletSpawner.position, bulletSpawner.rotation);
            audioSource.PlayOneShot(fireSFX, volume);
            fireTime = setFireTimer;
        }
    }
}
