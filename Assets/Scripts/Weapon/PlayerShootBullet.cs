﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShootBullet : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip fireSFX;

    [SerializeField]
    private float volume;

    [SerializeField]
    private float speed;

    [SerializeField]
    private GameObject shot;

    [SerializeField]
    private Transform bulletSpawner;

    private float fireRate = 0.25f;

    private float nextFire;
    // Start is called before the first frame update
    void Update()
    {
        if (Input.GetButton("Jump") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, bulletSpawner.position, bulletSpawner.rotation);
            audioSource.PlayOneShot(fireSFX, volume);
        }
    }

}
