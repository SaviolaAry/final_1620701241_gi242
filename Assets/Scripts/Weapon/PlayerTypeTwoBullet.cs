﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTypeTwoBullet : MonoBehaviour
{
    [SerializeField]
    private float speed;

    Rigidbody m_Rigidbody;
    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Rigidbody.velocity = transform.up * speed;
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }
}
