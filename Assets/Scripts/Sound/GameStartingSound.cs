﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameStartingSound : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip bgmSound;

    //-  -\\

    [SerializeField]
    private float volume = 0.01f;

    [SerializeField]
    private GameObject playerShip;

    [SerializeField]
    private GameObject enemyShip;

    [SerializeField]
    private Camera mainCam;

    [SerializeField]
    private Camera endGameCam;

    void Update()
    {
        if(Camera.current == mainCam)
        {
            audioSource.PlayOneShot(bgmSound, volume);
        }

        if(Camera.current == endGameCam)
        {
            audioSource.Stop();
        }
    }
}
